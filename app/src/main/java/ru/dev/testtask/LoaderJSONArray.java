package ru.dev.testtask;
import android.support.annotation.Nullable;
import org.json.JSONArray;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * Загрузчик данных
 */
public class LoaderJSONArray {

    static final String URL_STRING = "http://download.cdn.yandex.net/mobilization-2016/artists.json";
    static final String FILE_CACHE_NAME = "cache";

    JSONArray jsonArray;

    // Флаг, отвечающий за то, откуда брать данные по умолчанию.
    // true - данные будут браться из кэша, в случае не удачи, будет загрузка данных с сервера
    // false - данные берутся с сервера, ответ записывается кэш
    boolean fromCache;

    private File cacheDirectory;
    private File cacheDirectoryExternal;

    public LoaderJSONArray(File cacheDir, File cacheDirExternal, boolean fromCache) {
        this.cacheDirectory = cacheDir;
        this.cacheDirectoryExternal = cacheDirExternal;
        this.fromCache = fromCache;
    }

    /**
     * Загрузка данных
     * @throws Exception
     */
    public void load() throws Exception {
        try {
            if (fromCache) {
                // чтение данных из кэша
                jsonArray = readCache();
                if (jsonArray == null) {
                    throw new Exception("No cache file.");
                }
            } else {
                throw new Exception("fromCache = false;");
            }
        } catch (Exception e) {
            // запрос данных от сервера
            jsonArray = getJSONArray();

            // запись ответа в кэш
            writeCache(jsonArray.toString());
        }
    }

    /**
     * Получение данных в виде JSONArray по указанному url
     * @return JSONArray
     * @throws Exception
     */
    protected JSONArray getJSONArray() throws Exception {

        URL url = new URL(URL_STRING);

        // чтение данных
        Reader reader = new InputStreamReader(url.openStream());
        String jsonString = readStream(reader);

        return new JSONArray(jsonString);
    }

    /**
     * Чтение данных из кэша
     * @return JSONArray
     */
    @Nullable
    private JSONArray readCache() {
        // чтение данных из кэша
        try {
            // по умолчанию смотрим кэш на sd карте
            File cacheDir = cacheDirectoryExternal;
            if (cacheDir == null || !cacheDir.exists() || !cacheDir.isDirectory()) {
                cacheDir = cacheDirectory; // кэш внутренней памяти устройства
            }
            File fileCache = new File(cacheDir.getAbsolutePath() + "/" + FILE_CACHE_NAME);
            if (fileCache.exists() && !fileCache.isDirectory()) {
                String jsonString = readStream(new FileReader(fileCache));
                return new JSONArray(jsonString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Запись ответа от сервера в кэш
     * @param jsonString строка в формате json
     */
    private void writeCache(String jsonString) {
        // запись ответа в кэш
        try {
            // по умолчанию смотрим кэш на sd карте
            File cacheDir = cacheDirectoryExternal;
            if (cacheDir == null || !cacheDir.exists() || !cacheDir.isDirectory()) {
                cacheDir = cacheDirectory;
            }
            FileWriter writer = new FileWriter(cacheDir.getAbsolutePath() + "/" + FILE_CACHE_NAME, false);
            writer.write(jsonString);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Чтение данных из потока
     * @param reader Поток данных для чтения
     * @return String
     * @throws Exception
     */
    private static String readStream(Reader reader) throws Exception {

        BufferedReader bufferedReader = new BufferedReader(reader);

        String str = "";
        String line;
        while((line = bufferedReader.readLine()) != null) {
            str += line + "\n";
        }
        bufferedReader.close();
        reader.close();

        return str;
    }
}
