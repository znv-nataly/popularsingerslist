package ru.dev.testtask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import java.io.File;

/**
 * Загрузка изображения
 */
public class TaskImageLoad extends AsyncTask<Void, Void, Bitmap> {

    private Context context;
    private ImageView imageView;
    private boolean isImageOpen;

    private LoaderImage loader;

    public TaskImageLoad(Context context, String url, ImageView imageView, String nameArtist, boolean isSmall) {
        this.context = context;
        this.imageView = imageView;

        // если изображение большое, то при onClick оно будут открываться, например, через Галерею для просмотра
        this.isImageOpen = !isSmall;

        loader = new LoaderImage(url, context.getCacheDir(), context.getExternalCacheDir(), nameArtist, isSmall);
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            loader.load();
            return loader.bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        // после завершения выполнения doInBackground
        try {
            super.onPostExecute(bitmap);
            if (bitmap == null) {
                imageView.setImageResource(R.mipmap.white_background);
                return;
            }
            imageView.setImageBitmap(bitmap);

            // простая анимация - изменение прозрачности
            //Animation animation = AnimationUtils.loadAnimation(context, R.anim.alpha);
            //imageView.startAnimation(animation);

            if (isImageOpen) {
                // вешаем обработчик события Click для открытия изображения н-р через Галерею
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            File image = new File(loader.absoluteImagePath);
                            if (image.exists()) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + loader.absoluteImagePath), "image/*");
                                context.startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
