package ru.dev.testtask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityArtist extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_artist);
            overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

            // отображение стрелки "<-" (home) на верхней панели
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            Intent intent = getIntent();

            // имя исполнителя устанавливаем в качестве заголовка активити
            String nameArtist = intent.getStringExtra(TaskGetData.FIELD_NAME);
            setTitle(nameArtist);

            // загружаем обложку
            String urlBigCover = intent.getStringExtra(TaskGetData.FIELD_COVER);
            new TaskImageLoad(this, urlBigCover, (ImageView)findViewById(R.id.ivBigCover), nameArtist, false).execute();

            // заполнение полей
            ((TextView)findViewById(R.id.tvGenres)).setText(intent.getStringExtra(TaskGetData.FIELD_GENRES));
            ((TextView)findViewById(R.id.tvAlbums)).setText(intent.getStringExtra(TaskGetData.FIELD_ALBUMS));
            ((TextView)findViewById(R.id.tvTracks)).setText(intent.getStringExtra(TaskGetData.FIELD_TRACKS));
            ((TextView)findViewById(R.id.tvDescription)).setText(intent.getStringExtra(TaskGetData.FIELD_DESCRIPTION));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // обработка нажатия кнопки "Назад"
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }
}
