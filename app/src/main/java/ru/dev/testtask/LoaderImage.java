package ru.dev.testtask;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Загрузчик изображения
 */
public class LoaderImage {

    private File cacheDirectory;
    private File cacheDirectoryExternal;

    public Bitmap bitmap;

    // абсолютный путь к изображению, сохраненному в кэше
    public String absoluteImagePath;
    private String postfix;
    private String url;

    public LoaderImage(String url, File cacheDir, File cacheDirExternal, String nameArtist, boolean isSmall) {
        this.url = url;
        this.cacheDirectory = cacheDir;
        this.cacheDirectoryExternal = cacheDirExternal;
        this.absoluteImagePath = null;
        this.postfix = "/" + nameArtist + " (" + (isSmall ? "small" : "big") + ").jpg";
    }

    public void load() {
        try {
            // поиск изображения в кэше
            bitmap = getBitmapImageCache();
            if (bitmap != null) {
                return;
            }
            // загрузка с сервера
            bitmap = loadImageByUrl();

            // сохраняем изображение в кэше
            saveImageCache(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Загрузка изображения с сервера
     * @return Bitmap Изображение
     * @throws Exception
     */
    private Bitmap loadImageByUrl() throws Exception {

        URL urlConnection = new URL(url);
        HttpURLConnection connection = (HttpURLConnection)urlConnection.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        Bitmap bitmap = BitmapFactory.decodeStream(input);
        input.close();
        connection.disconnect();

        return bitmap;
    }

    /**
     * Поиск изображения в кэше
     * @return Bitmap
     */
    @Nullable
    private Bitmap getBitmapImageCache() {
        try {
            // проверка существования картинки в кэше, сначала смотрим на sd - карте
            File cacheDir = cacheDirectoryExternal;
            File image = null;
            if (cacheDir == null || !cacheDir.exists() || !cacheDir.isDirectory()) {
                cacheDir = cacheDirectory; // кэш внутренней памяти устройства
                if (cacheDir != null && cacheDir.exists() && cacheDir.isDirectory()) {
                    image = new File(cacheDir.getAbsolutePath() + postfix);
                }
            } else {
                image = new File(cacheDir.getAbsolutePath() + postfix);
            }

            if (image != null && image.exists() && !image.isDirectory()) {
                absoluteImagePath = image.getAbsolutePath();
                return BitmapFactory.decodeFile(image.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        absoluteImagePath = null;
        return null;
    }

    /**
     * Сохранение изображения в кэш
     * @param bitmap Изображение
     */
    private void saveImageCache(Bitmap bitmap) {
        try {
            if (bitmap == null)
                return;

            // по умолчанию пишем кэш на sd - карту
            File cacheDir = cacheDirectoryExternal;
            if (cacheDir == null || !cacheDir.exists() || !cacheDir.isDirectory()) {
                cacheDir = cacheDirectory;
            }
            String path = cacheDir.getAbsolutePath() + postfix;
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            absoluteImagePath = path;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
