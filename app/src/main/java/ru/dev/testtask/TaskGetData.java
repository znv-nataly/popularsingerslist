package ru.dev.testtask;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Получение данных по исполнителям и заполнение списка
 */
public class TaskGetData extends AsyncTask<Void, Void, JSONArray> {

    static final String FIELD_NAME = "name";
    static final String FIELD_GENRES = "genres";
    static final String FIELD_ALBUMS = "albums";
    static final String FIELD_TRACKS = "tracks";
    static final String FIELD_COVER = "cover";
    static final String FIELD_DESCRIPTION = "description";

    private Context context;
    private ListView lvArtists;
    private LinearLayout llLoadingPanel;
    private String textError;

    private LoaderJSONArray loader;

    public TaskGetData(Context context, ListView lvArtists, LinearLayout llLoadingPanel, boolean fromCache) {
        this.context = context;
        this.lvArtists = lvArtists;
        this.llLoadingPanel = llLoadingPanel;

        loader = new LoaderJSONArray(context.getCacheDir(), context.getExternalCacheDir(), fromCache);
    }

    @Override
    protected JSONArray doInBackground(Void... params) {
        try {
            loader.load();
            return loader.jsonArray;
        } catch (Exception e) {
            e.printStackTrace();
            textError = e.getMessage();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        // после завершения выполнения doInBackground
        try {
            super.onPostExecute(jsonArray);
            llLoadingPanel.setVisibility(View.GONE);
            if (textError != null && textError.length() > 0) {
                Toast.makeText(context, context.getResources().getString(R.string.errorGetData) + ". " + textError, Toast.LENGTH_LONG).show();
                return;
            }
            fillListViewArtists();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.errorGetData) + ". " + textError, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Формирование списка исполнителей
     */
    private void fillListViewArtists() {
        try {
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
            HashMap<String, String> map;
            JSONArray names;
            JSONObject jsonObject;
            String nameField;
            for (int i = 0; i < loader.jsonArray.length(); i++) {
                try {
                    jsonObject = (JSONObject) loader.jsonArray.get(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                    continue;
                }
                names = jsonObject.names();
                map = new HashMap<>();
                for (int j = 0; j < names.length(); j++) {
                    try {
                        nameField = names.get(j).toString();
                        map.put(nameField, jsonObject.getString(nameField));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                arrayList.add(map);
            }
            String[] from = new String[] { FIELD_NAME, FIELD_GENRES, FIELD_ALBUMS, FIELD_TRACKS, FIELD_COVER, FIELD_DESCRIPTION };
            int[] to = new int[] { R.id.tvName, R.id.tvGenres, R.id.tvAlbums, R.id.tvTracks, R.id.ivCover, R.id.tvDescription };

            SimpleAdapter adapter = new SimpleAdapter(context, arrayList, R.layout.item, from, to);
            adapter.setViewBinder(new ViewBinderArtists(context));
            lvArtists.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.errorFillListView), Toast.LENGTH_SHORT).show();
        }
    }
}
