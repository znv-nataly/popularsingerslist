package ru.dev.testtask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final int MENU_REFRESH_ID = 1;

    private LinearLayout llLoadingPanel;
    private ListView lvArtists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            llLoadingPanel = (LinearLayout) findViewById(R.id.llLoadingPanel);
            lvArtists = (ListView)findViewById(R.id.lvArtists);

            // обработчик клика на элемент списка
            lvArtists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // передача данных в другое activity
                    Intent intent = new Intent(getApplication(), ActivityArtist.class);
                    intent.putExtra(TaskGetData.FIELD_NAME, ((TextView)view.findViewById(R.id.tvName)).getText());
                    intent.putExtra(TaskGetData.FIELD_COVER, ((TextView)view.findViewById(R.id.tvUrlBigCover)).getText());
                    intent.putExtra(TaskGetData.FIELD_GENRES, ((TextView)view.findViewById(R.id.tvGenres)).getText());
                    intent.putExtra(TaskGetData.FIELD_ALBUMS, ((TextView)view.findViewById(R.id.tvAlbums)).getText());
                    intent.putExtra(TaskGetData.FIELD_TRACKS, ((TextView)view.findViewById(R.id.tvTracks)).getText());
                    intent.putExtra(TaskGetData.FIELD_DESCRIPTION, ((TextView)view.findViewById(R.id.tvDescription)).getText());
                    startActivity(intent);
                }
            });

            // выполнение задачи загрузки данных для списка (сначала читаем данные из кэша, если их там нет, то тогда идет обращение к серверу)
            new TaskGetData(this, lvArtists, llLoadingPanel, true).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            menu.add(0, MENU_REFRESH_ID, 0, "").setIcon(android.R.drawable.stat_notify_sync_noanim).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            return super.onCreateOptionsMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case MENU_REFRESH_ID:
                    llLoadingPanel.setVisibility(View.VISIBLE);
                    // выполнение задачи загрузки данных для списка (сразу обращаемся к серверу, миную кэш)
                    new TaskGetData(this, lvArtists, llLoadingPanel, false).execute();
                    break;
            }
            return super.onOptionsItemSelected(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}
