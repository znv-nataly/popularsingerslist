package ru.dev.testtask;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Определяет вид элемента списка исполнителей
 */
public class ViewBinderArtists implements SimpleAdapter.ViewBinder {

    private Context context;

    public ViewBinderArtists(Context context) {
        this.context = context;
    }

    @Override
    public boolean setViewValue(View view, Object data, String textRepresentation) {
        // устанавливаем значения для View-компонентов элемента списка
        try {
            String dataString = String.valueOf(data);
            switch (view.getId()) {
                case R.id.tvGenres:
                    try {
                        // жанры представлены в виде json массива
                        JSONArray genresJSONArray;
                        genresJSONArray = new JSONArray(dataString);
                        Object[] genresArray = new Object[genresJSONArray.length()];
                        for (int i = 0; i < genresJSONArray.length(); i++) {
                            genresArray[i] = genresJSONArray.get(i).toString();
                        }
                        ((TextView)view).setText(TextUtils.join(", ", genresArray));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        ((TextView)view).setText("");
                    }
                    break;
                case R.id.tvAlbums:
                    int albumsCount = Integer.valueOf(String.valueOf(data));
                    // определяем склонение
                    if (albumsCount >= 5 && albumsCount <= 20) {
                        ((TextView)view).setText(dataString + " альбомов");
                    } else if (albumsCount % 10 == 1 && albumsCount != 11) {
                        ((TextView)view).setText(dataString + " альбом");
                    } else if (albumsCount % 10 >= 2 && albumsCount % 10 <= 4) {
                        ((TextView)view).setText(dataString + " альбома");
                    } else {
                        ((TextView)view).setText(dataString + " альбомов");
                    }
                    break;
                case R.id.tvTracks:
                    int tracksCount = Integer.valueOf(dataString);
                    // определяем склонение
                    if (tracksCount >= 5 && tracksCount <= 20) {
                        ((TextView)view).setText(tracksCount + " песен");
                    } else if (tracksCount % 10 == 1 && tracksCount != 11) {
                        ((TextView)view).setText(tracksCount + " песня");
                    } else if (tracksCount % 10 >= 2 && tracksCount % 10 <= 4) {
                        ((TextView)view).setText(tracksCount + " песни");
                    } else {
                        ((TextView)view).setText(tracksCount + " песен");
                    }
                    break;
                case R.id.ivCover:
                    // загрузка изображения
                    try {
                        ImageView ivCover = (ImageView) view;
                        JSONObject jsonObject = new JSONObject(dataString);

                        // url маленького изображения обложки
                        String url = (String) jsonObject.get("small");
                        String nameArtist = ((TextView)((LinearLayout)ivCover.getParent()).findViewById(R.id.tvName)).getText().toString();
                        new TaskImageLoad(context, url, ((ImageView) view), nameArtist, true).execute();

                        // url большого изображения обложки
                        String urlBigCover = (String)jsonObject.get("big");
                        ((TextView)((LinearLayout)ivCover.getParent()).findViewById(R.id.tvUrlBigCover)).setText(urlBigCover);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        ((ImageView) view).setImageResource(R.mipmap.ic_music);
                    }
                    break;
                default:
                    ((TextView)view).setText(dataString);

            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
