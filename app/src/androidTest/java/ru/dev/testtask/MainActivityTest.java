package ru.dev.testtask;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;

import org.hamcrest.Matchers;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;

/**
 * Тестирование MainActivity
 * https://m.habrahabr.ru/post/212425/
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    @MediumTest
    public void testMenuItemRefreshClick() {
        // тестирование нажатие пункта меню "Обновить"
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(ViewMatchers.withId(MainActivity.MENU_REFRESH_ID)).perform(ViewActions.click());
    }

    @MediumTest
    public void testListViewItemClick() {
        // тестирование клика на элемент списка
        onData(Matchers.anything()).inAdapterView(ViewMatchers.withId(R.id.lvArtists)).atPosition(0).perform(ViewActions.click());
    }
}
