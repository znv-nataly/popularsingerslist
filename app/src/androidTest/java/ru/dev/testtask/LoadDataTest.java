package ru.dev.testtask;
import android.content.Context;
import android.net.ConnectivityManager;
import android.test.AndroidTestCase;

import junit.framework.Assert;
import org.junit.Test;

import java.io.File;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class LoadDataTest extends AndroidTestCase {

    Context context;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        context = getContext();
    }

    public void testLoadJSONArrayEnabledInternetConnection() throws Exception {
        if (isOnline()) {
            // тест при наличии интернет - соединения
            File cacheDir = context.getCacheDir();
            File cacheDirExternal = context.getExternalCacheDir();
            LoaderJSONArray loader = new LoaderJSONArray(cacheDir, cacheDirExternal, false);
            loader.load();
            assertNotNull(loader.jsonArray);
        }
    }

    public void testLoadJSONArrayDisabledInternetConnection() throws Exception {
        // тест при отсутствии интернет - соединения
        if (!isOnline()) {
            try {
                LoaderJSONArray loader = new LoaderJSONArray(context.getCacheDir(), context.getExternalCacheDir(), false);
                loader.load();
                Assert.fail("Should be an exception");
            } catch (Exception e) {
                // success
            }
        }
    }

    public void testLoadImageEnabledInternetConnection() throws Exception {
        if (isOnline()) {
            // тест при наличии интернет - соединения
            String url = "http://avatars.yandex.net/get-music-content/15ae00fc.p.2915/300x300";
            String nameArtist = "Ne-Yo";
            LoaderImage loader = new LoaderImage(url, context.getCacheDir(), context.getExternalCacheDir(), nameArtist, false);
            loader.load();
            assertNotNull(loader.bitmap);
        }
    }

    /**
     * Проверка наличия интернет соединения
     * @return boolean
     */
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}