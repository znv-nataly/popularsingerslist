package ru.dev.testtask;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import static android.support.test.espresso.Espresso.onView;

/**
 * Тестирование ActivityArtist
 */
public class ActivityArtistTest extends ActivityInstrumentationTestCase2<ActivityArtist> {

    public ActivityArtistTest() {
        super(ActivityArtist.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    @MediumTest
    public void testBigCoverClick() {
        // тестирование клика по изображению
        onView(ViewMatchers.withId(R.id.ivBigCover)).perform(ViewActions.click());
    }
}
